// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "AimState"),
	Walk_State UMETA(DisplayName = "WalkState"),
	Run_State UMETA(DisplayName = "RunState"),
	Sprint_State UMETA(DisplayName = "SprintState")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Movement")
	float AimSpeed = 300.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Movement")
	float WalkSpeed = 400.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Movement")
	float RunSpeed = 600.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Movement")
	float SprintSpeed = 1000.f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponInfo")
	float WeaponDamage = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponInfo")
	float RateOfFire = 0.5f;
};

UCLASS()
class TOPDOWNSHOOTER_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};