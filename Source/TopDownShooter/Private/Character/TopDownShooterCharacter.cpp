// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Character/TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	MinCameraLength = 300.f;
	MaxCameraLength = 1200.f;
	CamBoomLength = 1000.f;
}

void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	MovementTick(DeltaSeconds);
	CharacterUpdate();

	CameraBoom->TargetArmLength = UKismetMathLibrary::FInterpTo(CameraBoom->TargetArmLength, CamBoomLength, DeltaSeconds, 3.f);

	if (CursorToWorld != nullptr)
	{
		if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
}

void ATopDownShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	SetupPlayerInputComponent(InputComponent);
}

void ATopDownShooterCharacter::SetupPlayerInputComponent(UInputComponent * PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATopDownShooterCharacter::InputAxisX);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ATopDownShooterCharacter::InputAxisY);

	PlayerInputComponent->BindAction(TEXT("ZoomIn"), IE_Pressed, this, &ATopDownShooterCharacter::ZoomIn);
	PlayerInputComponent->BindAction(TEXT("ZoomOut"), IE_Pressed, this, &ATopDownShooterCharacter::ZoomOut);

	PlayerInputComponent->BindAction(TEXT("Walking"), IE_Pressed, this, &ATopDownShooterCharacter::StartWalking);
	PlayerInputComponent->BindAction(TEXT("Walking"), IE_Released, this, &ATopDownShooterCharacter::StopWalking);

	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &ATopDownShooterCharacter::StartSprint);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &ATopDownShooterCharacter::StopSprint);
}

void ATopDownShooterCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATopDownShooterCharacter::InputAxisY(float Value)
{
	AxisY = Value;

}

void ATopDownShooterCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.f, 0.f, 0.f), AxisX);
	AddMovementInput(FVector(0.f, 1.f, 0.f), AxisY);

	APlayerController * myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	if (myController)
	{
		FHitResult ResultHit;
		myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
		SetActorRotation(FQuat(FRotator(0.f, UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw, 0.f)));
	}
}

void ATopDownShooterCharacter::CharacterUpdate()
{
	float ResSpeed = 600.f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		if (CanSprint()) 
		{
			ResSpeed = MovementInfo.SprintSpeed;
		}
		else 
		{
			ResSpeed = MovementInfo.RunSpeed;
		}
		
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATopDownShooterCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();
}

void ATopDownShooterCharacter::StartWalking()
{
	ChangeMovementState(EMovementState::Walk_State);
}

void ATopDownShooterCharacter::StopWalking()
{
	if (MovementState == EMovementState::Walk_State)
	{
		ChangeMovementState(EMovementState::Run_State);
	}
}

void ATopDownShooterCharacter::StartSprint()
{
	ChangeMovementState(EMovementState::Sprint_State);
}

void ATopDownShooterCharacter::StopSprint()
{
	if (MovementState == EMovementState::Sprint_State)
	{
		ChangeMovementState(EMovementState::Run_State);
	}
}

bool ATopDownShooterCharacter::CanSprint()
{
	float RotationYaw = GetActorRotation().Yaw;
	float DirectionYaw = UKismetMathLibrary::DegAtan2(AxisY, AxisX);
	if (RotationYaw >= DirectionYaw - 15.f && RotationYaw <= DirectionYaw + 15.f) return true;
	return false;
}

void ATopDownShooterCharacter::ZoomIn()
{
	ChangeZoom(-200.f);
}

void ATopDownShooterCharacter::ZoomOut()
{
	ChangeZoom(200.f);
}

void ATopDownShooterCharacter::ChangeZoom(float deltaZoom)
{
	CamBoomLength = FMath::Clamp(CameraBoom->TargetArmLength + deltaZoom, MinCameraLength, MaxCameraLength);
}
